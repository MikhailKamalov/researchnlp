import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.Sentence;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.trees.*;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.io.IOException;
import java.sql.*;
import java.text.BreakIterator;
import java.util.Collection;
import java.util.Iterator;
import java.util.Locale;

/**
 * Created by mikhail on 18.01.15.
 */
public class ResearchNLP {

    private static final String DB_URL = "jdbc:mysql://localhost/testSet";


    private static final String USER = "root";
    private static final String PASS = "";

    public static void main(String[] args) throws IOException, SQLException {

        Connection conn = null;
        Statement stmt = null;
        conn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery("select * from testSet.medline_ct_2011");


        LexicalizedParser lp = LexicalizedParser.loadModel(
                "edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz",
                "-maxLength", "80", "-retainTmpSubcategories");
        TreebankLanguagePack tlp = new PennTreebankLanguagePack();
        GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();

       while(result.next()) {
           String text = result.getString("doc_body");//"Single intraoperative intravenous Co-Amoxiclav versus postoperative full oral course in prevention of postadenotonsillectomy morbidity: a randomised clinical trial.\tABSTRACT:Adenotonsillectomy results in postoperative morbidity which otolaryngologists attempt to reduce by use of antibiotics. The regimes used as quite varied with some opting for a full oral course postoperatively while others prefer prophylactic doses. This randomised clinical trial done in Kenyatta National Hospital, Kenya had the aim of comparing the efficacy of Co-Amoxiclav given as a single intravenous dose with a full oral course in the prevention of post adenotonsillectomy morbidity.126 patients below 12 years scheduled to undergo adenotonsillectomy were randomised into two groups. 63 were given a single intravenous dose of Enhancin [Co-Amoxiclav] at induction while the remaining half received a five days oral course of the same postoperatively. All received oral Pacimol [Paracetamol] in the postoperative period. Analysis was done and comparison made between the two groups with regards to pain, fever and diet tolerated in the postoperative period with a follow up period of seven days.There was no statistical significant difference between the two groups with regards to postoperative pain, fever and diet tolerated. All had a P-value > 0.2. Postoperative pain was highest in the first postoperative day and reduced progressively to the lowest level on the 7th postoperative day. As pain reduced, patients were able to tolerate a more solid diet with all but 6 tolerating their usual diet. 4 patients developed fever in the 1st postoperative day which did not progress to the next day. One patient had fever on the 4th and 7th postoperative day and was admitted in the paediatrics' ward with a chest infection. All these patients with history of fever were in the group that was on oral postoperative Co-Amoxiclav.A single intraoperative dose of Co-Amoxiclav given intravenously at induction was found to be just as effective as a full oral course of the same given postoperatively in the prevention of post adenotonsillectomy morbidity. The prophylactic dose is favoured over the later as it is cheaper, ensures compliance and relieves off the need for refrigeration of the oral suspension as not all have access to refrigeration in low economy countries as ours.ClinicalTrials.gov: NCT01267942.";

           BreakIterator border = BreakIterator.getSentenceInstance(Locale.US);
        border.setText(text);
        int start = border.first();

            for (int end = border.next(); end != BreakIterator.DONE; start = end, end = border.next()) {
                System.out.println(text.substring(start, end));
                String[] words = text.substring(start, end).split(" ");

                    Tree parse = lp.apply(Sentence.toWordList(words));
                    GrammaticalStructure gs = gsf.newGrammaticalStructure(parse);
                    TreeGraphNode trees = gs.root();

                   TreeGraphNode[] tr = trees.children();
                    //System.out.println(tr[0]);
                    Collection<TypedDependency> tdl = gs.typedDependenciesCCprocessed();

                    Iterator iterator = tdl.iterator();
                    while (iterator.hasNext()) {
                    TypedDependency td = (TypedDependency) iterator.next();

                 //   System.out.println("!!!" + td.reln().getRelatedNodes().toString());
                      if (td.reln().getShortName().equals("prep")) {
                          System.out.println(td.toString());
                   //       CoreLabel.OutputFormat outputFormat = new CoreLabel.OutputFormat();
                          //String st = td.toString()
                              //
                      }
                    }


                System.out.println("!!!!!");
            }
        }

    }
}
