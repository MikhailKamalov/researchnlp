package ru.apmath.parser.probability;

import org.apache.lucene.codecs.TermVectorsReader;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mikhail on 31.12.14.
 */
public class Frequency {

    private static final String DB_URL = "jdbc:mysql://localhost/testSet";


    private static final String USER = "root";
    private static final String PASS = "";



    public static void main(String[] args) throws SQLException, IOException {

      //  Connection conn = null;
      //  conn = DriverManager.getConnection(DB_URL, USER, PASS);
      //  Statement statement = conn.createStatement();

        Directory directory = FSDirectory.open(new File("/home/mikhail/Documents/research/hadoopIndexing1New13/index-0"));
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexReader indReader = DirectoryReader.open(FSDirectory.open(new File("/home/mikhail/Documents/research/hadoopIndexing1New13/index-0")));
        Fields fields = MultiFields.getFields(reader);

        //PrintWriter out = new PrintWriter("WordsFrequency.txt");

        Terms terms = fields.terms("doc_body");


        TermsEnum iterator = terms.iterator(null);


        //  iterator
        //false
        System.out.println(terms.hasOffsets());
        //true
        System.out.println(terms.hasPositions());

        BytesRef byteRef = null;

        int j = 0;
        HashMap<Integer,Integer> map = new HashMap<Integer, Integer>();

        while ((byteRef = iterator.next()) != null) {
            String docTerm = byteRef.utf8ToString();

            j++;
            System.out.println(docTerm);
            DocsAndPositionsEnum docPosEnum = iterator.docsAndPositions(null, null, DocsAndPositionsEnum.FLAG_OFFSETS);
            int freq =0;
            // statement.execute("insert into library (id, word) values ("+j+",'"+docTerm+"')");
            while (DocIdSetIterator.NO_MORE_DOCS != docPosEnum.nextDoc()) {
                freq = docPosEnum.freq();
                if(!map.containsKey(Integer.valueOf(j))){
                    map.put(Integer.valueOf(j),Integer.valueOf(freq));
                }
                else{
                    Integer i = map.get(Integer.valueOf(j));
                    i=i+freq;
                    map.replace(Integer.valueOf(j), map.get(Integer.valueOf(j)), i);

                }

            }



            //out.close();


        }

        for(Integer key : map.keySet()){

            System.out.println(key + "   "+ map.get(key));
           // statement.execute("insert into word_frequency (word_id,frequency) values  ("+key.intValue()+","+map.get(key).intValue()+")");


        }

    }}

