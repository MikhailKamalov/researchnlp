package ru.apmath.parser.probability;

import org.apache.lucene.index.*;
import org.apache.lucene.search.DocIdSetIterator;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by Mishanya on 19/11/2014.
 */
public class Position {

    private static final String DB_URL = "jdbc:mysql://localhost/testSet";


    private static final String USER = "root";
    private static final String PASS = "";


    public static void main(String[] args) throws SQLException, IOException {


       Connection conn = null;
       conn = DriverManager.getConnection(DB_URL, USER, PASS);
       Statement statement = conn.createStatement();

        Directory directory = FSDirectory.open(new File("/home/mikhail/Documents/research/ngrammSimple"));
        DirectoryReader reader = DirectoryReader.open(directory);
        IndexReader indReader = DirectoryReader.open(FSDirectory.open(new File("/home/mikhail/Documents/research/ngrammSimple")));
        Fields fields = MultiFields.getFields(reader);
        PrintWriter out = new PrintWriter("WordsSimple.txt");

        Terms terms = fields.terms("doc_body");

        TermsEnum iterator = terms.iterator(null);

      //  iterator
        //false
        System.out.println(terms.hasOffsets());
        //true
        System.out.println(terms.hasPositions());

        BytesRef byteRef = null;
        Term term =null;

        int j = 0;

        while ((byteRef = iterator.next()) != null ) {
            String docTerm = byteRef.utf8ToString();
            j++;
            DocsAndPositionsEnum docPosEnum = iterator.docsAndPositions(null, null, DocsAndPositionsEnum.FLAG_OFFSETS);

            statement.execute("insert into library (id, word) values ("+j+",'"+docTerm+"')");
            while (DocIdSetIterator.NO_MORE_DOCS != docPosEnum.nextDoc()) {
                int freq = docPosEnum.freq();
                for (int i = 0; i < freq; i++) {
                    int position = docPosEnum.nextPosition();
                    int start = docPosEnum.startOffset();
                    int end = docPosEnum.endOffset();
                     //System.out.println(Integer.parseInt(indReader.document(docPosEnum.docID()).getField("doc_original_id").stringValue()));

                     statement.execute("insert into word_position (word_id,doc_id,position) values ("+j+","+Integer.parseInt(indReader.document(docPosEnum.docID()).getField("doc_original_id").stringValue())+","+position+")");
                     System.out.println(Integer.parseInt(indReader.document(docPosEnum.docID()).getField("doc_original_id").stringValue())+"|"+docTerm + "|" + position+"|"+iterator.docFreq());////" docId "+ Integer.parseInt(indReader.document(docPosEnum.docID()).getField("id").stringValue()) + " position " + position);

                }

            }

            out.println(docTerm+"||"+j);


        }

        out.close();


    }
}
