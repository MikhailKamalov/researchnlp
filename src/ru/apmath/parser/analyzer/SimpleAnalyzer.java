package ru.apmath.parser.analyzer;

import edu.stanford.nlp.trees.EnglishGrammaticalRelations;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.ngram.EdgeNGramTokenizer;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.ngram.NGramTokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.util.Version;

import java.io.Reader;
import java.util.Set;

/**
 * Created by mikhail on 30.12.14.
 */

public  class SimpleAnalyzer extends StopwordAnalyzerBase {

    private final boolean isStem;

    public SimpleAnalyzer(final boolean isStem) {
        super(Version.LUCENE_44, new CharArraySet(Version.LUCENE_44, StopAnalyzer.ENGLISH_STOP_WORDS_SET, true));
        this.isStem = isStem;
    }

    @Override
    protected Analyzer.TokenStreamComponents createComponents(final String fieldName, final Reader reader) {

        final Tokenizer source = new StandardTokenizer(matchVersion,reader);
        TokenStream result = new LowerCaseFilter(matchVersion, source);

        if (isStem) {
            result = new PorterStemFilter(result);
        }
        return new Analyzer.TokenStreamComponents(source, result);
    }
}



