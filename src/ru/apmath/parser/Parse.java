package ru.apmath.parser;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.StopFilter;
import org.apache.lucene.analysis.en.PorterStemFilter;
import org.apache.lucene.analysis.ngram.NGramTokenFilter;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharArraySet;
import org.apache.lucene.analysis.util.StopwordAnalyzerBase;
import org.apache.lucene.util.Version;

import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Created by mikhail on 30.12.14.
 */
public class Parse {



    private static final String DB_URL = "jdbc:mysql://localhost/testSet";


    private static final String USER = "root";
    private static final String PASS = "";


    public static void main(String[] args) throws IOException, SQLException {
        Scanner sc = new Scanner(new File("medline_clinicalTrials_docs_2011_from_Sep.txt"));

        Connection conn = null;
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
        Statement statement = conn.createStatement();

        BufferedReader reader = new BufferedReader(new FileReader("medline_clinicalTrials_docs_2011_from_Sep.txt"));
        String line;
        List<String> lines = new ArrayList<String>();
        while ((line = reader.readLine()) != null) {
            lines.add(line);
        }
        //если нужен массив то список можно запросто преобрпзовать
        String [] linesAsArray = lines.toArray(new String[lines.size()]);

        for (int i=0;i<linesAsArray.length;i++){

            String[] newArray = linesAsArray[i].split("\t");



            //statement.execute("insert into medline_ct_2011 (doc_place,doc_author,doc_title,doc_body,doc_topics,doc_date,doc_original_id) values ('"+newArray[0].replace('\'',' ')+"','"+newArray[1].replace('\'',' ')+"','"+newArray[2].replace('\'',' ')+"','"+newArray[3].replace('\'',' ')+"','"+newArray[4].replace('\'',' ')+"','"+newArray[5].replace('\'',' ')+"',"+Integer.parseInt(newArray[6])+")");
            for (int j=0; j<newArray.length;j++){
                System.out.println(newArray[j]);
            }
            System.out.println("!!!!");
        }
    }



    public  class SimpleAnalyzer extends StopwordAnalyzerBase {

        private final boolean isStem;

        public SimpleAnalyzer(final Set<String> stopWords, final boolean isStem) {
            super(Version.LUCENE_43, new CharArraySet(Version.LUCENE_43, stopWords, true));
            this.isStem = isStem;
        }

        @Override
        protected Analyzer.TokenStreamComponents createComponents(final String fieldName, final Reader reader) {

            final Tokenizer source = new StandardTokenizer(matchVersion, reader);
            TokenStream result = new LowerCaseFilter(matchVersion, source);

            result = new LowerCaseFilter(matchVersion, result);

            if (isStem) {
                result = new PorterStemFilter(result);
            }
            return new Analyzer.TokenStreamComponents(source, result);
        }
    }

}
