package ru.apmath.parser.mapr;


import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.SimpleAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.LongField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.util.Version;
/**
 * Created by mikhail on 06.01.15.
 */
public class IndexOutputFormat extends LuceneIndexOutputFormat<NullWritable, Text> {
    // create some lucene Fields. These can be anything you'd like, such as DocFields
    public static final String doc_original_id = "doc_original_id";
    public static final String doc_title = "doc_title";
    public static int id =0;
    public static final String doc_body = "doc_body";
    public static final String doc_topics = "doc_body";

    private final Field doc_original_idField = new TextField(doc_original_id, "", Field.Store.YES);
    private final Field doc_titleField = new TextField(doc_title, "", Field.Store.YES);
    private final Field doc_bodyField = new TextField(doc_body, "", Field.Store.YES);
    private final Field doc_topicsField = new TextField(doc_topics, "", Field.Store.YES);
    private final Document doc = new Document();

    public IndexOutputFormat() {
        doc.add(doc_original_idField);
        doc.add(doc_titleField);
        doc.add(doc_bodyField);
        doc.add(doc_topicsField);
    }

    @Override
    protected Document buildDocument(NullWritable userId, Text tweetText) throws IOException {
        String[] str = tweetText.toString().split("\t");


        doc_original_idField.setStringValue(str[6]);
        doc_titleField.setStringValue(str[2]);
        doc_bodyField.setStringValue(str[3]);
        doc_topicsField.setStringValue(str[4]);
        return doc;
    }

    // Provide an analyzer to use. If you don't want to use an analyzer
    // (if your data is pre-tokenized perhaps) you can simply not override this method.
    @Override
    protected Analyzer newAnalyzer(Configuration conf) {
        return new ru.apmath.parser.analyzer.SimpleAnalyzer(false);
    }
}
