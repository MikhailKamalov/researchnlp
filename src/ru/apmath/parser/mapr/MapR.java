package ru.apmath.parser.mapr;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.log4j.Logger;

import ru.apmath.parser.mapr.util.LoggerUtils;

import java.io.IOException;


/**
 * Created by mikhail on 05.01.15.
 */
public class MapR extends Configured implements Tool {

    private final Logger LOG = Logger.getLogger(MapR.class);

    public static void main(final String[] args) throws Exception {

        LoggerUtils.initLogger();

        Configuration configuration = new Configuration();

        Job job = Job.getInstance(configuration, "indexing");
       // job.setJobName("dfgdfgd");




        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(IndexOutputFormat.class);

        job.setMapperClass(IndexMapper.class);
        job.setReducerClass(IndexReducer.class);


        job.setOutputValueClass(Text.class);
        job.setOutputKeyClass(NullWritable.class);


        FileSystem hdfs = FileSystem.get(configuration);

        Path pathHdfs = new Path("/home/mikhail/Documents/research/hdfs");
        hdfs.mkdirs(pathHdfs);

        Path localFilePath = new Path("/home/mikhail/Documents/research/testMedHadoop.txt");

        Path hdfsFilePath = new Path(pathHdfs + "/dataFile1.txt");

        hdfs.copyFromLocalFile(localFilePath, hdfsFilePath);


        Path path1 = new Path("/home/mikhail/Documents/research/hadoopIndexing1New41");

        TextInputFormat.setInputPaths(job, hdfsFilePath);

        // Directory dir = FSDirectory.open(new File("/home/mikhail/Documents/research/hadoopIndexingNew"));

        // LuceneIndexOutputFormat.createIndexWriter(dir, new SimpleAnalyzer(org.apache.lucene.util.Version.LUCENE_44), 3);

        IndexOutputFormat.setOutputPath(job, path1);


        //job.setNumReduceTasks(0);

        job.waitForCompletion(true);
    }


    public int run(final String[] args) throws Exception {

        return 0;
    }

    private static class IndexMapper extends Mapper<LongWritable, Text, NullWritable, Text> {
        @Override
        protected void map(LongWritable key, Text value, Context context)
                throws IOException, InterruptedException {

            context.write(NullWritable.get(), value);
        }
    }

    private static class IndexReducer extends Reducer<NullWritable, Text, NullWritable, Text> {
        @Override
        protected void reduce(NullWritable key, Iterable<Text> values, Context context)
                throws IOException, InterruptedException {

            for (Text value : values) {
                context.write(NullWritable.get(), value);
            }
        }
    }
}



