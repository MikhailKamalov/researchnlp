package ru.apmath.parser.mapr;

/**
 * Created by mikhail on 06.01.15.
 */
import java.io.File;
import java.io.IOException;
import java.io.Reader;
import com.google.common.io.Files;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.apache.hadoop.mapreduce.RecordWriter;
import org.apache.hadoop.mapreduce.TaskAttemptContext;
import org.apache.hadoop.mapreduce.lib.output.FileOutputCommitter;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.LogByteSizeMergePolicy;
import org.apache.lucene.index.SerialMergeScheduler;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.NoLockFactory;
import org.apache.lucene.store.SimpleFSDirectory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class LuceneIndexOutputFormat<K, V> extends FileOutputFormat<K, V> {
    private static final Logger LOG = LoggerFactory.getLogger(LuceneIndexOutputFormat.class);

    protected abstract Document buildDocument(K key, V value) throws IOException;

    protected Analyzer newAnalyzer(Configuration conf) {
        return new NeverTokenizeAnalyzer();
    }

    protected Directory getDirectoryImplementation(File location) throws IOException {
        return new SimpleFSDirectory(location, NoLockFactory.getNoLockFactory());
    }
    public static IndexWriter createIndexWriter(Directory location, Analyzer analyzer) throws IOException {
        return createIndexWriter(location, analyzer, LogByteSizeMergePolicy.DEFAULT_MERGE_FACTOR);
    }
    public static IndexWriter createIndexWriter(Directory location, Analyzer analyzer, int mergeFactor)
            throws IOException {
        LOG.info("Creating IndexWriter with:\nDirectory: "
                + location
                + "\nAnalyzer: "
                + analyzer
                + "\nMerge Factor: " + mergeFactor);
        IndexWriterConfig idxConfig = new IndexWriterConfig(Version.LUCENE_40, analyzer);
        LogByteSizeMergePolicy mergePolicy = new LogByteSizeMergePolicy();
        mergePolicy.setMergeFactor(mergeFactor);
        //mergePolicy.setUseCompoundFile(false);
        idxConfig.setMergePolicy(mergePolicy);
        idxConfig.setMergeScheduler(new SerialMergeScheduler());
        IndexWriter writer = new IndexWriter(location, idxConfig);
        return writer;
    }
    @Override
    public RecordWriter<K, V> getRecordWriter(TaskAttemptContext job) throws IOException {
        FileOutputCommitter committer = (FileOutputCommitter) this.getOutputCommitter(job);
        File tmpDirFile = Files.createTempDir();
        Directory directory = getDirectoryImplementation(tmpDirFile);
        IndexWriter writer = createIndexWriter(directory, newAnalyzer(HadoopCompat.getConfiguration(job)));
        return new IndexRecordWriter(writer, committer, tmpDirFile);
    }
    private class IndexRecordWriter extends RecordWriter<K, V> {
        private IndexWriter writer;
        private FileOutputCommitter committer;
        private File tmpDirFile;
        private long recordsProcessed = 0;
        private IndexRecordWriter(IndexWriter writer, FileOutputCommitter committer, File tmpDirFile) {
            this.writer = writer;
            this.committer = committer;
            this.tmpDirFile = tmpDirFile;
        }
        @Override
        public void write(K key, V value) throws IOException {
            recordsProcessed++;
            if (recordsProcessed % 1000000 == 0) {
                LOG.info("Processing record " + recordsProcessed);
            }
            writer.addDocument(buildDocument(key, value));
        }
        @Override
        public void close(final TaskAttemptContext context) throws IOException, InterruptedException {
            TaskHeartbeatThread heartBeat = new TaskHeartbeatThread(context) {
                @Override
                public void progress() {
                    String[] filesLeft = tmpDirFile.list();
                    if (filesLeft != null) {
                        int remaining = filesLeft.length - 2;
                        LOG.info("Optimizing " + remaining + " segments");
                    } else {
                        LOG.info("Done optimizing segments, heartbeat thread still alive");
                    }
                }
            };
            try {
                LOG.info("Starting heartbeat thread");
                heartBeat.start();
                Path work = committer.getWorkPath();
                Path output = new Path(work, "index-"
                        + String.valueOf(HadoopCompat.getTaskAttemptID(context).getTaskID().getId()));
                writer.forceMerge(1);
                writer.close();
                FileSystem fs = FileSystem.get(HadoopCompat.getConfiguration(context));
                LOG.info("Copying index to HDFS...");
                if (!FileUtil.copy(tmpDirFile, fs, output, true, HadoopCompat.getConfiguration(context))) {
                    throw new IOException("Failed to copy local index to HDFS!");
                }
                LOG.info("Index written to: " + output);
            } catch (IOException e) {
                LOG.error("Error committing index", e);
                throw e;
            } finally {

                LOG.info("Stopping heartbeat thread");
                heartBeat.stop();
            }
        }
    }

    public static class NeverTokenizeAnalyzer extends Analyzer {
        @Override
        protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
            throw new UnsupportedOperationException();
        }
    }
   
    public static PathFilter newIndexDirFilter(Configuration conf) {
        return new PathFilters.CompositePathFilter(
                PathFilters.newExcludeFilesFilter(conf),
                PathFilters.EXCLUDE_HIDDEN_PATHS_FILTER,
                new PathFilter() {
                    @Override
                    public boolean accept(Path path) {
                        return path.getName().startsWith("index-");
                    }
                }
        );
    }
}
