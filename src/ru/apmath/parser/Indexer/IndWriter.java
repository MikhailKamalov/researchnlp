package ru.apmath.parser.Indexer;



import edu.stanford.nlp.io.StringOutputStream;
import org.apache.avro.Schema;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import ru.apmath.parser.analyzer.SimpleAnalyzer;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

import static org.apache.lucene.document.Field.Store.YES;

/**
 * Created by Mishanya on 25/11/2014.
 */
public class IndWriter {

    private static final String DB_URL = "jdbc:mysql://localhost/testSet";


    private static final String USER = "root";
    private static final String PASS = "";

    private static IndexWriter writer;

    public static void main(String[] args) throws IOException, SQLException {
        IndWriter indexWriter = new IndWriter("/home/mikhail/Documents/research/ngrammSimple");
        passage(getResultSet("select * from testSet.medline_ct_2011"));
        indexWriter.close();
    }


    public IndWriter(String fileDir) throws IOException {
        Set<String> stringSet = new LinkedHashSet<String>();
        stringSet.add("s");
        stringSet.add("the");
        stringSet.add("a");


        SimpleAnalyzer analyzer = new SimpleAnalyzer(false);
        Directory dir = FSDirectory.open(new File(fileDir));
        IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_43, analyzer);
        writer = new IndexWriter(dir, config);

        System.out.println("");

    }

    // добавление документа в индекс
    private static void indexArticle(ResultSet result) throws IOException, SQLException {
        Set<String> stringSet = new LinkedHashSet<String>();
        stringSet.add("s");
        stringSet.add("the");
        stringSet.add("a");

       Document doc = getDocument(result);


        String dos =new String();
        Set<String> str = new TreeSet<String>();


        Schema schema = new Schema.Parser().parse(dos);


        System.out.println("docId");
        writer.addDocument(doc, new SimpleAnalyzer(false));
    }


    public void close() throws IOException {

        writer.close();
    }

    // добавление полей в документ
    private static Document getDocument(ResultSet res) throws SQLException, IOException {
        System.out.println("Retrieved ID: [" + res.getString("doc_original_id") + "]");
        Document doc = new Document();
        doc.add(new Field("doc_original_id", res.getString("doc_original_id"),YES,TextField.Index.ANALYZED));
        doc.add(new Field("doc_topics", res.getString("doc_topics"),YES,TextField.Index.ANALYZED));
        doc.add(new Field("doc_body", res.getString("doc_body") , YES, TextField.Index.ANALYZED, TextField.TermVector.WITH_POSITIONS_OFFSETS));
        doc.add(new Field("doc_title", res.getString("doc_title") , YES, TextField.Index.ANALYZED, TextField.TermVector.WITH_POSITIONS_OFFSETS));

        return doc;

    }

    //проход по всем записям таблицы med
    private static void passage(ResultSet res) throws SQLException, IOException {
        while (res.next()) {
            indexArticle(res);
        }

    }

    private static ResultSet getResultSet(String query) throws SQLException {
        Connection conn = null;
        Statement stmt = null;
        conn = (Connection) DriverManager.getConnection(DB_URL, USER, PASS);
        Statement statement = conn.createStatement();
        ResultSet result = statement.executeQuery(query);
        return result;

    }

}
